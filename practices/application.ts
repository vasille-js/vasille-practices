import { App } from "vasille";

/*
    Vasille Application must be created be extending App class
 */

// Good practice
{
    class Application extends App {
        constructor() {
            super(document.body);
        }

        $compose() {
            this.$text('Content example');
        }
    }

    const myApp = new Application().$init();
}

// Bad practice
{
    const myApp = new App(document.body).$init();

    myApp.$text('Content example');
}

// Why?
`
It's incorrect to create any children outside of $compose function.
The both fragments of code will do the same.
`
