import {Reactive, Reference} from "vasille";

/*
    Reactive objects are used to update model data
    without additional logic, just declaring bindings
 */


// Good practice
{
    class ReactiveObject extends Reactive {
        field = this.$ref(0);
    }
}

// Bad Practice 1
{
    class ReactiveObject extends Reactive {
        field = new Reference(0);
    }
}

// Bad practice 2
{
    const ReactiveObject = {
        field: new Reference(0)
    };
}

// Why?
`
The reactive field described in bad practice is not auto-managed:
 * it keeps to be reactive on object disabling.
 * it is not destroyed by object destructor.
`
