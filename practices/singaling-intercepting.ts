import {Fragment, Interceptor, InterceptorNode, Signal} from "vasille";

/*
    Interceptor permits to connect a signal of child to a slot of another child
 */

// Common code

class Child1 extends Fragment {
    signal = new Signal<number>()

    $mounted() {
        this.signal.emit(23);
    }
}

class Child2 extends Fragment {
    slot = (x : number) => {
        console.log(x);
    }
}

// Good practice
{
    class Parent extends Fragment {
        $compose() {
            this.$create(
                new InterceptorNode<number>(),
                $ => $.slot.insert((node, interceptor) =>
                {
                node.$create(new Child1(), $ => interceptor.connect($.signal));
                node.$create(new Child2(), $ => interceptor.connect($.slot));
            }));
        }
    }
}

// Bad practice
{

    class Parent extends Fragment {
        interceptor = new Interceptor<number>();

        $compose() {
            this.$create(new Child1(), $ => this.interceptor.connect($.signal));
            this.$create(new Child2(), $ => this.interceptor.connect($.slot));
        }

        $destroy() {
            super.$destroy();
            this.interceptor.$destroy();
        }
    }
}

// Why?
`
The bad practice request a custom destructor
and don't limit the DOM to a fragment where the interceptor can be used.
`

