import {App, Fragment} from "vasille";

/*
    By default, the data model can be updated in any direction,
    The goal is to protect the parent node data.
 */

// Good practice
{
    class Child extends Fragment {
        field = this.$ref(0);

        $ready() {
            this.field.$ = 3;
        }
    }

    class Parent extends App {
        field = this.$ref(1);

        $compose() {
            this.$create(new Child, $ => $.field = this.$forward(this.field));
        }
    }

    const app = new Parent(document.body).$init();
}

// Bad practice
{
    class Child extends Fragment {
        field = this.$ref(0);

        $ready() {
            this.field.$ = 3;
        }
    }

    class Parent extends App {
        field = this.$ref(1);

        $compose() {
            this.$create(new Child, $ => $.field = $.$bind(v => v, this.field));
        }
    }

    const app = new Parent(document.body).$init();
}

// Why?
`
The both code will have the same result parent.field == 1, child.field == 3.
But the bad practice will use additional useless logic.
`
