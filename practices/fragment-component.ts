import {Component, Fragment, Slot, Tag} from "vasille";

/*
    Fragment are useful to create components with any number of children
    Component are useful to make additional manipulation on component root
 */

// Good practice
{
    class MyComponent1 extends Fragment {
        slot = new Slot()

        $compose() {
            this.$tag("div");
            this.$tag("div");
            this.slot.release(this);
        }
    }

    class MyComponent2 extends Fragment {
        slot = new Slot()

        $compose() {
            this.$create(new MyComponent1, child => {
                this.slot.release(child);
            });
        }
    }

    class MyComponent3 extends Fragment {
        slot = new Slot()

        $compose() {
            this.slot.release(this);
        }
    }

    class MyComponent4 extends Component {
        slot = new Slot<Tag>()

        $compose() {
            // div.className = 'class1'
            this.$tag("div", div => {
                div.$addClass('class1');
                this.slot.release(div);
            });
        }
    }

    class MyComponent5 extends Component {
        $compose() {
            this.$create(new MyComponent4, $ => $.slot.insert((node) => {
                // div.className = 'class1 class2'
                node.$addClass('class2');
            }));
        }
    }
}

// Why?
`
If component has some tags as root, it must extend Fragment class.
If component root is a fragment, it must extend Fragment class.
If component has no children, it must extend Fragment class.
If component has a tag or a component (which extends Component) as root, it must extend Component class.
`
