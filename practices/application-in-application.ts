import {App, AppNode} from "vasille";

/*
    Application as sub-application is useful to have different settings for
    fragments of application, which can be developed by individual teams.
 */

// Good practice
{
    class SubApp extends AppNode {
        $compose() {
            this.$text('this is a sub-app');
        }
    }

    class MainApp extends App {
        constructor() {
            super(document.body);
        }

        $compose() {
            this.$tag("div", div => {
                div.$create(new SubApp());
            });
        }
    }

    const main = new MainApp().$init();
}

// Bad practice
{
    class SubApp extends App {
        $compose() {
            this.$text('this is a sub-app');
        }
    }

    class MainApp extends App {
        constructor() {
            super(document.body);
        }

        $compose() {
            this.$tag("div", div => {
                new SubApp(div.node).$init();
            });
        }
    }

    const main = new MainApp().$init();
}

// Why?
`
This example creates a div node in main app and a text node in subapp.
The good practice is esthetically released and the subapp will be destroyed
when the main app will remove the host div node.
`
