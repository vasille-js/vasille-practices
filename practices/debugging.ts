import {App} from "vasille";

// Practice 1
{
    class Debugging extends App {

        constructor() {
            super(document.body, {
                debugUi: true
            });
        }

        ref = this.$ref(0);


        $compose() {
            this.$debug(this.$bind(v => `ref = ${v}`, this.ref));
        }
    }
}

// Practice 2
{
    class Debugging extends App {

        constructor() {
            super(document.body);
        }

        ref = this.$ref(0);


        $createWatchers() {
            this.$watch((v) => {
                console.log('ref =', v);
            }, this.ref);
        }
    }
}

// Why?
`
The practice 1 will generate a DOM comment and keep it up to date,
debug comments can be disabled in production builds.
The practice 2 will generate a console log on each value change,
use it if you really need a log, not just the current value.
`
