import {Component, Extension, Slot} from "vasille";

/*
    Extensions are similar to mixins, but its logic is completely isolated.
 */

// Practice
{
    class HideNodeExtension extends Extension {
        $compose() {
            this.$setStyle('display', 'none');
        }
    }

    class AddClickEventExtension extends Extension {
        $compose() {
            this.$onclick(ev => {
                console.log(ev.screenX, ev.screenY);
            });
        }
    }

    class Button extends Component {
        text = this.$ref("Text");
        buttonSlot = new Slot();
        spanSlot = new Slot();

        $compose() {
            this.$tag('button', button => {
                button.$tag('span', span => {
                    span.$text(this.text);
                    this.spanSlot.release(span);
                });
                this.buttonSlot.release(button);
            });
        }
    }

    class CustomButton extends Component {
        $compose() {
            this.$create(new Button, $ => {
                $.buttonSlot.insert(node => {
                    // Add click event to button using extension
                    node.$create(new AddClickEventExtension);
                });
                $.spanSlot.insert(node => {
                    // Hide the span element using extension
                    node.$create(new HideNodeExtension);
                });
            });
        }
    }
}
