import {Fragment} from "vasille";

/*
    A value can be updated manually and automatically in the same time,
    like css value which can be absolute, relative or 'auto'.
 */

// Good practice
{
    class Example extends Fragment {
        auto = this.$bind(v => v, this.$ref(0));
        absolute = this.$ref(0);
        finally = this.$point(this.absolute);

        $created() {
            // make value to be updated manually
            this.finally.point(this.absolute);
            // make value to be updated automatically
            this.finally.point(this.auto);
            // Change absolute value
            this.absolute.$ = 23;
        }
    }
}

// Bad practice
{
    class Example extends Fragment {
        auto = this.$bind(v => v, this.$ref(0));
        finnaly = this.$mirror(this.auto);

        $created() {
            // make value to be updated manually
            this.auto.disable();
            // make value to be updated automatically
            this.auto.enable();
            // Change absolute value
            this.finnaly.$ = 23;
        }
    }
}

// Why?
`
The good practice have the next advantages:
* It supports several auto values.
* It supports several absolute values.
* The absolute value is not lost after switching to auto.
* Changing the absolute value in automatically mode will trigger changes
 which can be canceled automatically.
`
