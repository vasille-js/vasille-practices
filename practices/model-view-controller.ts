import {ArrayModel, ArrayView, Fragment, MapModel, MapView, ObjectModel, ObjectView, SetModel, SetView} from "vasille";

/*
    All is simple, just use it
 */

{
    class Example extends Fragment {
        // models
        arrayModel = this.$register(new ArrayModel([1, 2, 3]));
        setModel = this.$register(new SetModel([1, 2, 3]));
        objectModel = this.$register(new ObjectModel({
            1: 1,
            2: 2
        }));
        mapModel = this.$register(new MapModel([
            [1, 1],
            [2, 2]
        ]));

        // views
        $compose() {

            this.$create(new ArrayView(this.arrayModel), $ => $.slot.insert((node, value) => {
                // create content here
            }));

            this.$create(new SetView(this.setModel), $ => $.slot.insert((node, value) => {
                // create content here
            }));

            this.$create(new ObjectView(this.objectModel), $ => $.slot.insert((node, value, key) => {
                // create content here
            }));

            this.$create(new MapView(this.mapModel), $ => $.slot.insert((node, value, key) => {
                // create content here
            }));
        }

        // controller
        // Your code which can manipulate models
    }
}
