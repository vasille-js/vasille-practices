import {Fragment, Signal} from "vasille";


/*
    Sending and receiving signals (events) make communication between components possible.
 */

// Good practice
{
    class Child extends Fragment {
        signal = new Signal<number>()

        $ready() {
            this.signal.emit(23);
        }
    }

    class Parent extends Fragment {
        buffer = 0
        slot = (x : number) => {
            this.buffer = x;
        }

        $compose() {
            this.$create(new Child, $ => $.signal.subscribe(this.slot));
        }
    }
}

// Bad practice
{
    class Child extends Fragment {
        signal = new Signal<number>()

        $ready() {
            this.signal.emit(23);
        }
    }

    class Parent extends Fragment {
        buffer = 0

        $compose() {
            this.$create(new Child, $ => $.signal.subscribe((x : number) => {
                this.buffer = x;
            }));
        }
    }
}

// Why?
`
The bad practice make impossible to use the slot (event handler) with a interceptor
and mix up the DOM generation code with logic.
`
